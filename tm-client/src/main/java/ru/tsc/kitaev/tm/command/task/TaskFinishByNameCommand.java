package ru.tsc.kitaev.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kitaev.tm.command.AbstractTaskCommand;
import ru.tsc.kitaev.tm.endpoint.Session;
import ru.tsc.kitaev.tm.endpoint.Task;
import ru.tsc.kitaev.tm.exception.entity.TaskNotFoundException;
import ru.tsc.kitaev.tm.util.TerminalUtil;

public final class TaskFinishByNameCommand extends AbstractTaskCommand {

    @NotNull
    @Override
    public String name() {
        return "task-finish-by-name";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Finish task by name...";
    }

    @Override
    public void execute() {
        @Nullable final Session session = serviceLocator.getSessionService().getSession();
        System.out.println("Enter name");
        @NotNull final String name = TerminalUtil.nextLine();
        serviceLocator.getTaskEndpoint().finishTaskByName(session, name);
    }

}
