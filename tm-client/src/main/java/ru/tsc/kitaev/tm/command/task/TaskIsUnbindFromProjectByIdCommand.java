package ru.tsc.kitaev.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kitaev.tm.command.AbstractTaskCommand;
import ru.tsc.kitaev.tm.endpoint.Session;
import ru.tsc.kitaev.tm.endpoint.Task;
import ru.tsc.kitaev.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.kitaev.tm.exception.entity.TaskNotFoundException;
import ru.tsc.kitaev.tm.util.TerminalUtil;

public final class TaskIsUnbindFromProjectByIdCommand extends AbstractTaskCommand {

    @NotNull
    @Override
    public String name() {
        return "tasks-unbind-from-project-by-id";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Unbind task from project by id...";
    }

    @Override
    public void execute() {
        @Nullable final Session session = serviceLocator.getSessionService().getSession();
        System.out.println("Enter project id");
        @NotNull final String projectId = TerminalUtil.nextLine();
        if (serviceLocator.getProjectEndpoint().findProjectById(session, projectId) == null) throw new ProjectNotFoundException();
        System.out.println("Enter task id");
        @NotNull final String taskId = TerminalUtil.nextLine();
        if (serviceLocator.getTaskEndpoint().findTaskById(session, taskId) == null) throw new TaskNotFoundException();
        serviceLocator.getProjectTaskEndpoint().unbindTaskById(session, projectId, taskId);
    }

}
