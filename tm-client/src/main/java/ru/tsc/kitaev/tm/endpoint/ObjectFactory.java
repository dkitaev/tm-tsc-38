
package ru.tsc.kitaev.tm.endpoint;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the ru.tsc.kitaev.tm.endpoint package. 
 * &lt;p&gt;An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _DataBackupLoad_QNAME = new QName("http://endpoint.tm.kitaev.tsc.ru/", "dataBackupLoad");
    private final static QName _DataBackupLoadResponse_QNAME = new QName("http://endpoint.tm.kitaev.tsc.ru/", "dataBackupLoadResponse");
    private final static QName _DataBackupSave_QNAME = new QName("http://endpoint.tm.kitaev.tsc.ru/", "dataBackupSave");
    private final static QName _DataBackupSaveResponse_QNAME = new QName("http://endpoint.tm.kitaev.tsc.ru/", "dataBackupSaveResponse");
    private final static QName _DataBase64Load_QNAME = new QName("http://endpoint.tm.kitaev.tsc.ru/", "dataBase64Load");
    private final static QName _DataBase64LoadResponse_QNAME = new QName("http://endpoint.tm.kitaev.tsc.ru/", "dataBase64LoadResponse");
    private final static QName _DataBase64Save_QNAME = new QName("http://endpoint.tm.kitaev.tsc.ru/", "dataBase64Save");
    private final static QName _DataBase64SaveResponse_QNAME = new QName("http://endpoint.tm.kitaev.tsc.ru/", "dataBase64SaveResponse");
    private final static QName _DataBinLoad_QNAME = new QName("http://endpoint.tm.kitaev.tsc.ru/", "dataBinLoad");
    private final static QName _DataBinLoadResponse_QNAME = new QName("http://endpoint.tm.kitaev.tsc.ru/", "dataBinLoadResponse");
    private final static QName _DataBinSave_QNAME = new QName("http://endpoint.tm.kitaev.tsc.ru/", "dataBinSave");
    private final static QName _DataBinSaveResponse_QNAME = new QName("http://endpoint.tm.kitaev.tsc.ru/", "dataBinSaveResponse");
    private final static QName _DataJsonLoadFasterXML_QNAME = new QName("http://endpoint.tm.kitaev.tsc.ru/", "dataJsonLoadFasterXML");
    private final static QName _DataJsonLoadFasterXMLResponse_QNAME = new QName("http://endpoint.tm.kitaev.tsc.ru/", "dataJsonLoadFasterXMLResponse");
    private final static QName _DataJsonLoadJaxB_QNAME = new QName("http://endpoint.tm.kitaev.tsc.ru/", "dataJsonLoadJaxB");
    private final static QName _DataJsonLoadJaxBResponse_QNAME = new QName("http://endpoint.tm.kitaev.tsc.ru/", "dataJsonLoadJaxBResponse");
    private final static QName _DataJsonSaveFasterXML_QNAME = new QName("http://endpoint.tm.kitaev.tsc.ru/", "dataJsonSaveFasterXML");
    private final static QName _DataJsonSaveFasterXMLResponse_QNAME = new QName("http://endpoint.tm.kitaev.tsc.ru/", "dataJsonSaveFasterXMLResponse");
    private final static QName _DataJsonSaveJaxB_QNAME = new QName("http://endpoint.tm.kitaev.tsc.ru/", "dataJsonSaveJaxB");
    private final static QName _DataJsonSaveJaxBResponse_QNAME = new QName("http://endpoint.tm.kitaev.tsc.ru/", "dataJsonSaveJaxBResponse");
    private final static QName _DataXmlLoadFasterXML_QNAME = new QName("http://endpoint.tm.kitaev.tsc.ru/", "dataXmlLoadFasterXML");
    private final static QName _DataXmlLoadFasterXMLResponse_QNAME = new QName("http://endpoint.tm.kitaev.tsc.ru/", "dataXmlLoadFasterXMLResponse");
    private final static QName _DataXmlLoadJaxB_QNAME = new QName("http://endpoint.tm.kitaev.tsc.ru/", "dataXmlLoadJaxB");
    private final static QName _DataXmlLoadJaxBResponse_QNAME = new QName("http://endpoint.tm.kitaev.tsc.ru/", "dataXmlLoadJaxBResponse");
    private final static QName _DataXmlSaveFasterXML_QNAME = new QName("http://endpoint.tm.kitaev.tsc.ru/", "dataXmlSaveFasterXML");
    private final static QName _DataXmlSaveFasterXMLResponse_QNAME = new QName("http://endpoint.tm.kitaev.tsc.ru/", "dataXmlSaveFasterXMLResponse");
    private final static QName _DataXmlSaveJaxB_QNAME = new QName("http://endpoint.tm.kitaev.tsc.ru/", "dataXmlSaveJaxB");
    private final static QName _DataXmlSaveJaxBResponse_QNAME = new QName("http://endpoint.tm.kitaev.tsc.ru/", "dataXmlSaveJaxBResponse");
    private final static QName _DataYamlLoadFasterXML_QNAME = new QName("http://endpoint.tm.kitaev.tsc.ru/", "dataYamlLoadFasterXML");
    private final static QName _DataYamlLoadFasterXMLResponse_QNAME = new QName("http://endpoint.tm.kitaev.tsc.ru/", "dataYamlLoadFasterXMLResponse");
    private final static QName _DataYamlSaveFasterXML_QNAME = new QName("http://endpoint.tm.kitaev.tsc.ru/", "dataYamlSaveFasterXML");
    private final static QName _DataYamlSaveFasterXMLResponse_QNAME = new QName("http://endpoint.tm.kitaev.tsc.ru/", "dataYamlSaveFasterXMLResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: ru.tsc.kitaev.tm.endpoint
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link DataBackupLoad }
     * 
     */
    public DataBackupLoad createDataBackupLoad() {
        return new DataBackupLoad();
    }

    /**
     * Create an instance of {@link DataBackupLoadResponse }
     * 
     */
    public DataBackupLoadResponse createDataBackupLoadResponse() {
        return new DataBackupLoadResponse();
    }

    /**
     * Create an instance of {@link DataBackupSave }
     * 
     */
    public DataBackupSave createDataBackupSave() {
        return new DataBackupSave();
    }

    /**
     * Create an instance of {@link DataBackupSaveResponse }
     * 
     */
    public DataBackupSaveResponse createDataBackupSaveResponse() {
        return new DataBackupSaveResponse();
    }

    /**
     * Create an instance of {@link DataBase64Load }
     * 
     */
    public DataBase64Load createDataBase64Load() {
        return new DataBase64Load();
    }

    /**
     * Create an instance of {@link DataBase64LoadResponse }
     * 
     */
    public DataBase64LoadResponse createDataBase64LoadResponse() {
        return new DataBase64LoadResponse();
    }

    /**
     * Create an instance of {@link DataBase64Save }
     * 
     */
    public DataBase64Save createDataBase64Save() {
        return new DataBase64Save();
    }

    /**
     * Create an instance of {@link DataBase64SaveResponse }
     * 
     */
    public DataBase64SaveResponse createDataBase64SaveResponse() {
        return new DataBase64SaveResponse();
    }

    /**
     * Create an instance of {@link DataBinLoad }
     * 
     */
    public DataBinLoad createDataBinLoad() {
        return new DataBinLoad();
    }

    /**
     * Create an instance of {@link DataBinLoadResponse }
     * 
     */
    public DataBinLoadResponse createDataBinLoadResponse() {
        return new DataBinLoadResponse();
    }

    /**
     * Create an instance of {@link DataBinSave }
     * 
     */
    public DataBinSave createDataBinSave() {
        return new DataBinSave();
    }

    /**
     * Create an instance of {@link DataBinSaveResponse }
     * 
     */
    public DataBinSaveResponse createDataBinSaveResponse() {
        return new DataBinSaveResponse();
    }

    /**
     * Create an instance of {@link DataJsonLoadFasterXML }
     * 
     */
    public DataJsonLoadFasterXML createDataJsonLoadFasterXML() {
        return new DataJsonLoadFasterXML();
    }

    /**
     * Create an instance of {@link DataJsonLoadFasterXMLResponse }
     * 
     */
    public DataJsonLoadFasterXMLResponse createDataJsonLoadFasterXMLResponse() {
        return new DataJsonLoadFasterXMLResponse();
    }

    /**
     * Create an instance of {@link DataJsonLoadJaxB }
     * 
     */
    public DataJsonLoadJaxB createDataJsonLoadJaxB() {
        return new DataJsonLoadJaxB();
    }

    /**
     * Create an instance of {@link DataJsonLoadJaxBResponse }
     * 
     */
    public DataJsonLoadJaxBResponse createDataJsonLoadJaxBResponse() {
        return new DataJsonLoadJaxBResponse();
    }

    /**
     * Create an instance of {@link DataJsonSaveFasterXML }
     * 
     */
    public DataJsonSaveFasterXML createDataJsonSaveFasterXML() {
        return new DataJsonSaveFasterXML();
    }

    /**
     * Create an instance of {@link DataJsonSaveFasterXMLResponse }
     * 
     */
    public DataJsonSaveFasterXMLResponse createDataJsonSaveFasterXMLResponse() {
        return new DataJsonSaveFasterXMLResponse();
    }

    /**
     * Create an instance of {@link DataJsonSaveJaxB }
     * 
     */
    public DataJsonSaveJaxB createDataJsonSaveJaxB() {
        return new DataJsonSaveJaxB();
    }

    /**
     * Create an instance of {@link DataJsonSaveJaxBResponse }
     * 
     */
    public DataJsonSaveJaxBResponse createDataJsonSaveJaxBResponse() {
        return new DataJsonSaveJaxBResponse();
    }

    /**
     * Create an instance of {@link DataXmlLoadFasterXML }
     * 
     */
    public DataXmlLoadFasterXML createDataXmlLoadFasterXML() {
        return new DataXmlLoadFasterXML();
    }

    /**
     * Create an instance of {@link DataXmlLoadFasterXMLResponse }
     * 
     */
    public DataXmlLoadFasterXMLResponse createDataXmlLoadFasterXMLResponse() {
        return new DataXmlLoadFasterXMLResponse();
    }

    /**
     * Create an instance of {@link DataXmlLoadJaxB }
     * 
     */
    public DataXmlLoadJaxB createDataXmlLoadJaxB() {
        return new DataXmlLoadJaxB();
    }

    /**
     * Create an instance of {@link DataXmlLoadJaxBResponse }
     * 
     */
    public DataXmlLoadJaxBResponse createDataXmlLoadJaxBResponse() {
        return new DataXmlLoadJaxBResponse();
    }

    /**
     * Create an instance of {@link DataXmlSaveFasterXML }
     * 
     */
    public DataXmlSaveFasterXML createDataXmlSaveFasterXML() {
        return new DataXmlSaveFasterXML();
    }

    /**
     * Create an instance of {@link DataXmlSaveFasterXMLResponse }
     * 
     */
    public DataXmlSaveFasterXMLResponse createDataXmlSaveFasterXMLResponse() {
        return new DataXmlSaveFasterXMLResponse();
    }

    /**
     * Create an instance of {@link DataXmlSaveJaxB }
     * 
     */
    public DataXmlSaveJaxB createDataXmlSaveJaxB() {
        return new DataXmlSaveJaxB();
    }

    /**
     * Create an instance of {@link DataXmlSaveJaxBResponse }
     * 
     */
    public DataXmlSaveJaxBResponse createDataXmlSaveJaxBResponse() {
        return new DataXmlSaveJaxBResponse();
    }

    /**
     * Create an instance of {@link DataYamlLoadFasterXML }
     * 
     */
    public DataYamlLoadFasterXML createDataYamlLoadFasterXML() {
        return new DataYamlLoadFasterXML();
    }

    /**
     * Create an instance of {@link DataYamlLoadFasterXMLResponse }
     * 
     */
    public DataYamlLoadFasterXMLResponse createDataYamlLoadFasterXMLResponse() {
        return new DataYamlLoadFasterXMLResponse();
    }

    /**
     * Create an instance of {@link DataYamlSaveFasterXML }
     * 
     */
    public DataYamlSaveFasterXML createDataYamlSaveFasterXML() {
        return new DataYamlSaveFasterXML();
    }

    /**
     * Create an instance of {@link DataYamlSaveFasterXMLResponse }
     * 
     */
    public DataYamlSaveFasterXMLResponse createDataYamlSaveFasterXMLResponse() {
        return new DataYamlSaveFasterXMLResponse();
    }

    /**
     * Create an instance of {@link Session }
     * 
     */
    public Session createSession() {
        return new Session();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DataBackupLoad }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link DataBackupLoad }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.kitaev.tsc.ru/", name = "dataBackupLoad")
    public JAXBElement<DataBackupLoad> createDataBackupLoad(DataBackupLoad value) {
        return new JAXBElement<DataBackupLoad>(_DataBackupLoad_QNAME, DataBackupLoad.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DataBackupLoadResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link DataBackupLoadResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.kitaev.tsc.ru/", name = "dataBackupLoadResponse")
    public JAXBElement<DataBackupLoadResponse> createDataBackupLoadResponse(DataBackupLoadResponse value) {
        return new JAXBElement<DataBackupLoadResponse>(_DataBackupLoadResponse_QNAME, DataBackupLoadResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DataBackupSave }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link DataBackupSave }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.kitaev.tsc.ru/", name = "dataBackupSave")
    public JAXBElement<DataBackupSave> createDataBackupSave(DataBackupSave value) {
        return new JAXBElement<DataBackupSave>(_DataBackupSave_QNAME, DataBackupSave.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DataBackupSaveResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link DataBackupSaveResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.kitaev.tsc.ru/", name = "dataBackupSaveResponse")
    public JAXBElement<DataBackupSaveResponse> createDataBackupSaveResponse(DataBackupSaveResponse value) {
        return new JAXBElement<DataBackupSaveResponse>(_DataBackupSaveResponse_QNAME, DataBackupSaveResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DataBase64Load }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link DataBase64Load }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.kitaev.tsc.ru/", name = "dataBase64Load")
    public JAXBElement<DataBase64Load> createDataBase64Load(DataBase64Load value) {
        return new JAXBElement<DataBase64Load>(_DataBase64Load_QNAME, DataBase64Load.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DataBase64LoadResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link DataBase64LoadResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.kitaev.tsc.ru/", name = "dataBase64LoadResponse")
    public JAXBElement<DataBase64LoadResponse> createDataBase64LoadResponse(DataBase64LoadResponse value) {
        return new JAXBElement<DataBase64LoadResponse>(_DataBase64LoadResponse_QNAME, DataBase64LoadResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DataBase64Save }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link DataBase64Save }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.kitaev.tsc.ru/", name = "dataBase64Save")
    public JAXBElement<DataBase64Save> createDataBase64Save(DataBase64Save value) {
        return new JAXBElement<DataBase64Save>(_DataBase64Save_QNAME, DataBase64Save.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DataBase64SaveResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link DataBase64SaveResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.kitaev.tsc.ru/", name = "dataBase64SaveResponse")
    public JAXBElement<DataBase64SaveResponse> createDataBase64SaveResponse(DataBase64SaveResponse value) {
        return new JAXBElement<DataBase64SaveResponse>(_DataBase64SaveResponse_QNAME, DataBase64SaveResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DataBinLoad }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link DataBinLoad }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.kitaev.tsc.ru/", name = "dataBinLoad")
    public JAXBElement<DataBinLoad> createDataBinLoad(DataBinLoad value) {
        return new JAXBElement<DataBinLoad>(_DataBinLoad_QNAME, DataBinLoad.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DataBinLoadResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link DataBinLoadResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.kitaev.tsc.ru/", name = "dataBinLoadResponse")
    public JAXBElement<DataBinLoadResponse> createDataBinLoadResponse(DataBinLoadResponse value) {
        return new JAXBElement<DataBinLoadResponse>(_DataBinLoadResponse_QNAME, DataBinLoadResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DataBinSave }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link DataBinSave }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.kitaev.tsc.ru/", name = "dataBinSave")
    public JAXBElement<DataBinSave> createDataBinSave(DataBinSave value) {
        return new JAXBElement<DataBinSave>(_DataBinSave_QNAME, DataBinSave.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DataBinSaveResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link DataBinSaveResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.kitaev.tsc.ru/", name = "dataBinSaveResponse")
    public JAXBElement<DataBinSaveResponse> createDataBinSaveResponse(DataBinSaveResponse value) {
        return new JAXBElement<DataBinSaveResponse>(_DataBinSaveResponse_QNAME, DataBinSaveResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DataJsonLoadFasterXML }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link DataJsonLoadFasterXML }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.kitaev.tsc.ru/", name = "dataJsonLoadFasterXML")
    public JAXBElement<DataJsonLoadFasterXML> createDataJsonLoadFasterXML(DataJsonLoadFasterXML value) {
        return new JAXBElement<DataJsonLoadFasterXML>(_DataJsonLoadFasterXML_QNAME, DataJsonLoadFasterXML.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DataJsonLoadFasterXMLResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link DataJsonLoadFasterXMLResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.kitaev.tsc.ru/", name = "dataJsonLoadFasterXMLResponse")
    public JAXBElement<DataJsonLoadFasterXMLResponse> createDataJsonLoadFasterXMLResponse(DataJsonLoadFasterXMLResponse value) {
        return new JAXBElement<DataJsonLoadFasterXMLResponse>(_DataJsonLoadFasterXMLResponse_QNAME, DataJsonLoadFasterXMLResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DataJsonLoadJaxB }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link DataJsonLoadJaxB }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.kitaev.tsc.ru/", name = "dataJsonLoadJaxB")
    public JAXBElement<DataJsonLoadJaxB> createDataJsonLoadJaxB(DataJsonLoadJaxB value) {
        return new JAXBElement<DataJsonLoadJaxB>(_DataJsonLoadJaxB_QNAME, DataJsonLoadJaxB.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DataJsonLoadJaxBResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link DataJsonLoadJaxBResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.kitaev.tsc.ru/", name = "dataJsonLoadJaxBResponse")
    public JAXBElement<DataJsonLoadJaxBResponse> createDataJsonLoadJaxBResponse(DataJsonLoadJaxBResponse value) {
        return new JAXBElement<DataJsonLoadJaxBResponse>(_DataJsonLoadJaxBResponse_QNAME, DataJsonLoadJaxBResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DataJsonSaveFasterXML }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link DataJsonSaveFasterXML }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.kitaev.tsc.ru/", name = "dataJsonSaveFasterXML")
    public JAXBElement<DataJsonSaveFasterXML> createDataJsonSaveFasterXML(DataJsonSaveFasterXML value) {
        return new JAXBElement<DataJsonSaveFasterXML>(_DataJsonSaveFasterXML_QNAME, DataJsonSaveFasterXML.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DataJsonSaveFasterXMLResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link DataJsonSaveFasterXMLResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.kitaev.tsc.ru/", name = "dataJsonSaveFasterXMLResponse")
    public JAXBElement<DataJsonSaveFasterXMLResponse> createDataJsonSaveFasterXMLResponse(DataJsonSaveFasterXMLResponse value) {
        return new JAXBElement<DataJsonSaveFasterXMLResponse>(_DataJsonSaveFasterXMLResponse_QNAME, DataJsonSaveFasterXMLResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DataJsonSaveJaxB }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link DataJsonSaveJaxB }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.kitaev.tsc.ru/", name = "dataJsonSaveJaxB")
    public JAXBElement<DataJsonSaveJaxB> createDataJsonSaveJaxB(DataJsonSaveJaxB value) {
        return new JAXBElement<DataJsonSaveJaxB>(_DataJsonSaveJaxB_QNAME, DataJsonSaveJaxB.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DataJsonSaveJaxBResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link DataJsonSaveJaxBResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.kitaev.tsc.ru/", name = "dataJsonSaveJaxBResponse")
    public JAXBElement<DataJsonSaveJaxBResponse> createDataJsonSaveJaxBResponse(DataJsonSaveJaxBResponse value) {
        return new JAXBElement<DataJsonSaveJaxBResponse>(_DataJsonSaveJaxBResponse_QNAME, DataJsonSaveJaxBResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DataXmlLoadFasterXML }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link DataXmlLoadFasterXML }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.kitaev.tsc.ru/", name = "dataXmlLoadFasterXML")
    public JAXBElement<DataXmlLoadFasterXML> createDataXmlLoadFasterXML(DataXmlLoadFasterXML value) {
        return new JAXBElement<DataXmlLoadFasterXML>(_DataXmlLoadFasterXML_QNAME, DataXmlLoadFasterXML.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DataXmlLoadFasterXMLResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link DataXmlLoadFasterXMLResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.kitaev.tsc.ru/", name = "dataXmlLoadFasterXMLResponse")
    public JAXBElement<DataXmlLoadFasterXMLResponse> createDataXmlLoadFasterXMLResponse(DataXmlLoadFasterXMLResponse value) {
        return new JAXBElement<DataXmlLoadFasterXMLResponse>(_DataXmlLoadFasterXMLResponse_QNAME, DataXmlLoadFasterXMLResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DataXmlLoadJaxB }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link DataXmlLoadJaxB }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.kitaev.tsc.ru/", name = "dataXmlLoadJaxB")
    public JAXBElement<DataXmlLoadJaxB> createDataXmlLoadJaxB(DataXmlLoadJaxB value) {
        return new JAXBElement<DataXmlLoadJaxB>(_DataXmlLoadJaxB_QNAME, DataXmlLoadJaxB.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DataXmlLoadJaxBResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link DataXmlLoadJaxBResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.kitaev.tsc.ru/", name = "dataXmlLoadJaxBResponse")
    public JAXBElement<DataXmlLoadJaxBResponse> createDataXmlLoadJaxBResponse(DataXmlLoadJaxBResponse value) {
        return new JAXBElement<DataXmlLoadJaxBResponse>(_DataXmlLoadJaxBResponse_QNAME, DataXmlLoadJaxBResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DataXmlSaveFasterXML }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link DataXmlSaveFasterXML }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.kitaev.tsc.ru/", name = "dataXmlSaveFasterXML")
    public JAXBElement<DataXmlSaveFasterXML> createDataXmlSaveFasterXML(DataXmlSaveFasterXML value) {
        return new JAXBElement<DataXmlSaveFasterXML>(_DataXmlSaveFasterXML_QNAME, DataXmlSaveFasterXML.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DataXmlSaveFasterXMLResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link DataXmlSaveFasterXMLResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.kitaev.tsc.ru/", name = "dataXmlSaveFasterXMLResponse")
    public JAXBElement<DataXmlSaveFasterXMLResponse> createDataXmlSaveFasterXMLResponse(DataXmlSaveFasterXMLResponse value) {
        return new JAXBElement<DataXmlSaveFasterXMLResponse>(_DataXmlSaveFasterXMLResponse_QNAME, DataXmlSaveFasterXMLResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DataXmlSaveJaxB }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link DataXmlSaveJaxB }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.kitaev.tsc.ru/", name = "dataXmlSaveJaxB")
    public JAXBElement<DataXmlSaveJaxB> createDataXmlSaveJaxB(DataXmlSaveJaxB value) {
        return new JAXBElement<DataXmlSaveJaxB>(_DataXmlSaveJaxB_QNAME, DataXmlSaveJaxB.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DataXmlSaveJaxBResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link DataXmlSaveJaxBResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.kitaev.tsc.ru/", name = "dataXmlSaveJaxBResponse")
    public JAXBElement<DataXmlSaveJaxBResponse> createDataXmlSaveJaxBResponse(DataXmlSaveJaxBResponse value) {
        return new JAXBElement<DataXmlSaveJaxBResponse>(_DataXmlSaveJaxBResponse_QNAME, DataXmlSaveJaxBResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DataYamlLoadFasterXML }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link DataYamlLoadFasterXML }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.kitaev.tsc.ru/", name = "dataYamlLoadFasterXML")
    public JAXBElement<DataYamlLoadFasterXML> createDataYamlLoadFasterXML(DataYamlLoadFasterXML value) {
        return new JAXBElement<DataYamlLoadFasterXML>(_DataYamlLoadFasterXML_QNAME, DataYamlLoadFasterXML.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DataYamlLoadFasterXMLResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link DataYamlLoadFasterXMLResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.kitaev.tsc.ru/", name = "dataYamlLoadFasterXMLResponse")
    public JAXBElement<DataYamlLoadFasterXMLResponse> createDataYamlLoadFasterXMLResponse(DataYamlLoadFasterXMLResponse value) {
        return new JAXBElement<DataYamlLoadFasterXMLResponse>(_DataYamlLoadFasterXMLResponse_QNAME, DataYamlLoadFasterXMLResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DataYamlSaveFasterXML }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link DataYamlSaveFasterXML }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.kitaev.tsc.ru/", name = "dataYamlSaveFasterXML")
    public JAXBElement<DataYamlSaveFasterXML> createDataYamlSaveFasterXML(DataYamlSaveFasterXML value) {
        return new JAXBElement<DataYamlSaveFasterXML>(_DataYamlSaveFasterXML_QNAME, DataYamlSaveFasterXML.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DataYamlSaveFasterXMLResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link DataYamlSaveFasterXMLResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.kitaev.tsc.ru/", name = "dataYamlSaveFasterXMLResponse")
    public JAXBElement<DataYamlSaveFasterXMLResponse> createDataYamlSaveFasterXMLResponse(DataYamlSaveFasterXMLResponse value) {
        return new JAXBElement<DataYamlSaveFasterXMLResponse>(_DataYamlSaveFasterXMLResponse_QNAME, DataYamlSaveFasterXMLResponse.class, null, value);
    }

}
