package ru.tsc.kitaev.tm.endpoint;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.ws.Action;
import javax.xml.ws.RequestWrapper;
import javax.xml.ws.ResponseWrapper;

/**
 * This class was generated by Apache CXF 3.4.2
 * 2021-11-15T13:57:01.540+03:00
 * Generated source version: 3.4.2
 *
 */
@WebService(targetNamespace = "http://endpoint.tm.kitaev.tsc.ru/", name = "ProjectTaskEndpoint")
@XmlSeeAlso({ObjectFactory.class})
public interface ProjectTaskEndpoint {

    @WebMethod
    @Action(input = "http://endpoint.tm.kitaev.tsc.ru/ProjectTaskEndpoint/findTaskByProjectIdRequest", output = "http://endpoint.tm.kitaev.tsc.ru/ProjectTaskEndpoint/findTaskByProjectIdResponse")
    @RequestWrapper(localName = "findTaskByProjectId", targetNamespace = "http://endpoint.tm.kitaev.tsc.ru/", className = "ru.tsc.kitaev.tm.endpoint.FindTaskByProjectId")
    @ResponseWrapper(localName = "findTaskByProjectIdResponse", targetNamespace = "http://endpoint.tm.kitaev.tsc.ru/", className = "ru.tsc.kitaev.tm.endpoint.FindTaskByProjectIdResponse")
    @WebResult(name = "return", targetNamespace = "")
    public java.util.List<ru.tsc.kitaev.tm.endpoint.Task> findTaskByProjectId(

        @WebParam(name = "session", targetNamespace = "")
        ru.tsc.kitaev.tm.endpoint.Session session,
        @WebParam(name = "projectId", targetNamespace = "")
        java.lang.String projectId
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.kitaev.tsc.ru/ProjectTaskEndpoint/removeByNameRequest", output = "http://endpoint.tm.kitaev.tsc.ru/ProjectTaskEndpoint/removeByNameResponse")
    @RequestWrapper(localName = "removeByName", targetNamespace = "http://endpoint.tm.kitaev.tsc.ru/", className = "ru.tsc.kitaev.tm.endpoint.RemoveByName")
    @ResponseWrapper(localName = "removeByNameResponse", targetNamespace = "http://endpoint.tm.kitaev.tsc.ru/", className = "ru.tsc.kitaev.tm.endpoint.RemoveByNameResponse")
    public void removeByName(

        @WebParam(name = "session", targetNamespace = "")
        ru.tsc.kitaev.tm.endpoint.Session session,
        @WebParam(name = "name", targetNamespace = "")
        java.lang.String name
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.kitaev.tsc.ru/ProjectTaskEndpoint/removeByIndexRequest", output = "http://endpoint.tm.kitaev.tsc.ru/ProjectTaskEndpoint/removeByIndexResponse")
    @RequestWrapper(localName = "removeByIndex", targetNamespace = "http://endpoint.tm.kitaev.tsc.ru/", className = "ru.tsc.kitaev.tm.endpoint.RemoveByIndex")
    @ResponseWrapper(localName = "removeByIndexResponse", targetNamespace = "http://endpoint.tm.kitaev.tsc.ru/", className = "ru.tsc.kitaev.tm.endpoint.RemoveByIndexResponse")
    public void removeByIndex(

        @WebParam(name = "session", targetNamespace = "")
        ru.tsc.kitaev.tm.endpoint.Session session,
        @WebParam(name = "index", targetNamespace = "")
        java.lang.Integer index
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.kitaev.tsc.ru/ProjectTaskEndpoint/removeByIdRequest", output = "http://endpoint.tm.kitaev.tsc.ru/ProjectTaskEndpoint/removeByIdResponse")
    @RequestWrapper(localName = "removeById", targetNamespace = "http://endpoint.tm.kitaev.tsc.ru/", className = "ru.tsc.kitaev.tm.endpoint.RemoveById")
    @ResponseWrapper(localName = "removeByIdResponse", targetNamespace = "http://endpoint.tm.kitaev.tsc.ru/", className = "ru.tsc.kitaev.tm.endpoint.RemoveByIdResponse")
    public void removeById(

        @WebParam(name = "session", targetNamespace = "")
        ru.tsc.kitaev.tm.endpoint.Session session,
        @WebParam(name = "projectId", targetNamespace = "")
        java.lang.String projectId
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.kitaev.tsc.ru/ProjectTaskEndpoint/unbindTaskByIdRequest", output = "http://endpoint.tm.kitaev.tsc.ru/ProjectTaskEndpoint/unbindTaskByIdResponse")
    @RequestWrapper(localName = "unbindTaskById", targetNamespace = "http://endpoint.tm.kitaev.tsc.ru/", className = "ru.tsc.kitaev.tm.endpoint.UnbindTaskById")
    @ResponseWrapper(localName = "unbindTaskByIdResponse", targetNamespace = "http://endpoint.tm.kitaev.tsc.ru/", className = "ru.tsc.kitaev.tm.endpoint.UnbindTaskByIdResponse")
    public void unbindTaskById(

        @WebParam(name = "session", targetNamespace = "")
        ru.tsc.kitaev.tm.endpoint.Session session,
        @WebParam(name = "projectId", targetNamespace = "")
        java.lang.String projectId,
        @WebParam(name = "taskId", targetNamespace = "")
        java.lang.String taskId
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.kitaev.tsc.ru/ProjectTaskEndpoint/bindTaskByIdRequest", output = "http://endpoint.tm.kitaev.tsc.ru/ProjectTaskEndpoint/bindTaskByIdResponse")
    @RequestWrapper(localName = "bindTaskById", targetNamespace = "http://endpoint.tm.kitaev.tsc.ru/", className = "ru.tsc.kitaev.tm.endpoint.BindTaskById")
    @ResponseWrapper(localName = "bindTaskByIdResponse", targetNamespace = "http://endpoint.tm.kitaev.tsc.ru/", className = "ru.tsc.kitaev.tm.endpoint.BindTaskByIdResponse")
    public void bindTaskById(

        @WebParam(name = "session", targetNamespace = "")
        ru.tsc.kitaev.tm.endpoint.Session session,
        @WebParam(name = "projectId", targetNamespace = "")
        java.lang.String projectId,
        @WebParam(name = "taskId", targetNamespace = "")
        java.lang.String taskId
    );
}
