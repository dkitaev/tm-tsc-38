package ru.tsc.kitaev.tm.repository;

import org.jetbrains.annotations.Nullable;
import ru.tsc.kitaev.tm.api.repository.ISessionRepository;
import ru.tsc.kitaev.tm.endpoint.Session;

public class SessionRepository implements ISessionRepository {

    @Nullable
    private Session session;

    @Override
    @Nullable
    public Session getSession() {
        return session;
    }

    @Override
    public void setSession(@Nullable final Session session) {
        this.session = session;
    }

}
