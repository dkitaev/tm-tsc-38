package ru.tsc.kitaev.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.tsc.kitaev.tm.endpoint.*;
import ru.tsc.kitaev.tm.marker.SoapCategory;

public class UserEndpointTest {

    @NotNull
    private final SessionEndpoint sessionEndpoint;

    @NotNull
    private final UserEndpoint userEndpoint;

    @NotNull
    private Session session;

    public UserEndpointTest() {
        @NotNull final SessionEndpointService sessionEndpointService = new SessionEndpointService();
        sessionEndpoint = sessionEndpointService.getSessionEndpointPort();
        @NotNull final UserEndpointService userEndpointService = new UserEndpointService();
        userEndpoint = userEndpointService.getUserEndpointPort();
    }

    @Before
    public void before() {
        session = sessionEndpoint.openSession("test", "test");
    }

    @Test
    @Category(SoapCategory.class)
    public void findByIdTest() {
        Assert.assertNotNull(session);
        Assert.assertNotNull(userEndpoint.findById(session));
    }

    @Test
    @Category(SoapCategory.class)
    public void setPasswordUserTest() {
        @NotNull final String oldPassword = userEndpoint.findById(session).getPasswordHash();
        userEndpoint.setPassword(session, "newPassword");
        @NotNull final String newPassword = userEndpoint.findById(session).getPasswordHash();
        Assert.assertNotEquals(oldPassword, newPassword);
        userEndpoint.setPassword(session, "test");
    }

    @Test
    @Category(SoapCategory.class)
    public void updateByIdUser() {
        @NotNull final User oldUser = userEndpoint.findById(session);
        @NotNull final String lastName = "lastName",
                firstName = "firstName",
                middleName = "middleName";
        userEndpoint.updateUser(session, firstName, lastName, middleName);
        @NotNull final User updatedUser = userEndpoint.findById(session);
        Assert.assertEquals(lastName, updatedUser.getLastName());
        Assert.assertEquals(firstName, updatedUser.getFirstName());
        Assert.assertEquals(middleName, updatedUser.getMiddleName());
    }

    @After
    public void after() {
        sessionEndpoint.closeSession(session);
    }

}
