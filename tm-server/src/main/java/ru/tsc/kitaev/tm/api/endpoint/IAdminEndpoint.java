package ru.tsc.kitaev.tm.api.endpoint;

import org.jetbrains.annotations.Nullable;
import ru.tsc.kitaev.tm.model.Session;

import javax.jws.WebMethod;
import javax.jws.WebParam;

public interface IAdminEndpoint {

    @WebMethod
    void dataBackupLoad(
            @WebParam(name = "session", partName = "session") @Nullable final Session session
    );

    @WebMethod
    void dataBackupSave(
            @WebParam(name = "session", partName = "session") @Nullable final Session session
    );

    @WebMethod
    void dataBase64Load(
            @WebParam(name = "session", partName = "session") @Nullable final Session session
    );

    @WebMethod
    void dataBase64Save(
            @WebParam(name = "session", partName = "session") @Nullable final Session session
    );

    @WebMethod
    void dataBinLoad(
            @WebParam(name = "session", partName = "session") @Nullable final Session session
    );

    @WebMethod
    void dataBinSave(
            @WebParam(name = "session", partName = "session") @Nullable final Session session
    );

    @WebMethod
    void dataJsonLoadFasterXML(
            @WebParam(name = "session", partName = "session") @Nullable final Session session
    );

    @WebMethod
    void dataJsonSaveFasterXML(
            @WebParam(name = "session", partName = "session") @Nullable final Session session
    );

    @WebMethod
    void dataJsonLoadJaxB(
            @WebParam(name = "session", partName = "session") @Nullable final Session session
    );

    @WebMethod
    void dataJsonSaveJaxB(
            @WebParam(name = "session", partName = "session") @Nullable final Session session
    );

    @WebMethod
    void dataXmlLoadFasterXML(
            @WebParam(name = "session", partName = "session") @Nullable final Session session
    );

    @WebMethod
    void dataXmlSaveFasterXML(
            @WebParam(name = "session", partName = "session") @Nullable final Session session
    );

    @WebMethod
    void dataXmlLoadJaxB(
            @WebParam(name = "session", partName = "session") @Nullable final Session session
    );

    @WebMethod
    void dataXmlSaveJaxB(
            @WebParam(name = "session", partName = "session") @Nullable final Session session
    );

    @WebMethod
    void dataYamlLoadFasterXML(
            @WebParam(name = "session", partName = "session") @Nullable final Session session
    );

    @WebMethod
    void dataYamlSaveFasterXML(
            @WebParam(name = "session", partName = "session") @Nullable final Session session
    );

}
