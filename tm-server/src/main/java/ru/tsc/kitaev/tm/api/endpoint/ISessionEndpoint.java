package ru.tsc.kitaev.tm.api.endpoint;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kitaev.tm.dto.Result;
import ru.tsc.kitaev.tm.model.Session;

import javax.jws.WebMethod;
import javax.jws.WebParam;

public interface ISessionEndpoint {

    @WebMethod
    @NotNull
    @SneakyThrows
    Session openSession(
            @WebParam(name = "login", partName = "login") @Nullable final String login,
            @WebParam(name = "password", partName = "password") @Nullable final String password
    );

    @WebMethod
    @NotNull
    Result closeSession(@WebParam(name = "session", partName = "session") @NotNull final Session session);

}
