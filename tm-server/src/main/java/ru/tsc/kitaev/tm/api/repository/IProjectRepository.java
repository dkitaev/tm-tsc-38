package ru.tsc.kitaev.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kitaev.tm.enumerated.Status;
import ru.tsc.kitaev.tm.model.Project;

import java.sql.SQLException;
import java.util.List;

public interface IProjectRepository extends IOwnerRepository<Project> {

    void add(@NotNull final String userId, @NotNull final Project project) throws SQLException;

    void addAll(@NotNull final List<Project> projects) throws SQLException;

    @NotNull
    Project findByName(@NotNull final String userId, @NotNull final String name) throws SQLException;

    void removeByName(@NotNull final String userId, @NotNull final String name) throws SQLException;

    void updateById(
            @NotNull final String userId,
            @NotNull final String id,
            @NotNull final String name,
            @NotNull final String description
    ) throws SQLException;

    void updateByIndex(
            @NotNull final String userId,
            @NotNull final Integer index,
            @NotNull final String name,
            @NotNull final String description
    ) throws SQLException;

    void startById(@NotNull final String userId, @NotNull final String id) throws SQLException;

    void startByIndex(@NotNull final String userId, @NotNull final Integer index) throws SQLException;

    void startByName(@NotNull final String userId, @NotNull final String name) throws SQLException;

    void finishById(@NotNull final String userId, @NotNull final String id) throws SQLException;

    void finishByIndex(@NotNull final String userId, @NotNull final Integer index) throws SQLException;

    void finishByName(@NotNull final String userId, @NotNull final String name) throws SQLException;

    void changeStatusById(
            @NotNull final String userId,
            @NotNull final String id,
            @NotNull final Status status
    ) throws SQLException;

    void changeStatusByIndex(
            @NotNull final String userId,
            @NotNull final Integer index,
            @NotNull final Status status
    ) throws SQLException;

    void changeStatusByName(
            @NotNull final String userId,
            @NotNull final String name,
            @NotNull final Status status
    ) throws SQLException;

}
