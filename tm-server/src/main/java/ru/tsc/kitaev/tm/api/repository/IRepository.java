package ru.tsc.kitaev.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kitaev.tm.model.AbstractEntity;

import java.sql.SQLException;
import java.util.Comparator;
import java.util.List;

public interface IRepository<E extends AbstractEntity> {

    void remove(@NotNull final E entity) throws SQLException;

    void clear() throws SQLException;

    @NotNull
    List<E> findAll() throws SQLException;

    @NotNull
    List<E> findAll(@NotNull final Comparator<E> comparator) throws SQLException;

    @NotNull
    E findById(@NotNull final String id) throws SQLException;

    @NotNull
    E findByIndex(@NotNull final Integer index) throws SQLException;

    void removeById(@NotNull final String id) throws SQLException;

    void removeByIndex(@NotNull final Integer index) throws SQLException;

    boolean existsById(@NotNull final String id) throws SQLException;

    boolean existsByIndex(final int index) throws SQLException;

    int getSize() throws SQLException;

}
