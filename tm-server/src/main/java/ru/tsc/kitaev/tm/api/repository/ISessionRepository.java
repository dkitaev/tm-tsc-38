package ru.tsc.kitaev.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import ru.tsc.kitaev.tm.model.Project;
import ru.tsc.kitaev.tm.model.Session;

import java.sql.SQLException;

public interface ISessionRepository extends IRepository<Session>{

    void add(@NotNull final Session session) throws SQLException;

    boolean exists(@NotNull final String id) throws SQLException;

}
