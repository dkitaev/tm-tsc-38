package ru.tsc.kitaev.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import ru.tsc.kitaev.tm.model.User;

import java.sql.SQLException;
import java.util.List;

public interface IUserRepository extends IRepository<User> {

    void add(@NotNull final User user) throws SQLException;

    void addAll(@NotNull final List<User> users) throws SQLException;

    void removeUser(@NotNull final User user) throws SQLException;

    @NotNull
    User findByLogin(@NotNull final String login) throws SQLException;

    @NotNull
    User findByEmail(@NotNull final String email) throws SQLException;

    void removeByLogin(@NotNull final String login) throws SQLException;

    void updatePassword(@NotNull final String id, @NotNull final String password) throws SQLException;

    void updateLockUserByLogin(@NotNull final String login, final boolean locked) throws SQLException;

    void updateUser(
            @NotNull final String id,
            @NotNull final String firstName,
            @NotNull final String lastName,
            @NotNull final String middleName
    ) throws SQLException;

    Boolean isLoginExists(@NotNull final String login) throws SQLException;

    Boolean isEmailExists(@NotNull final String email) throws SQLException;

}
