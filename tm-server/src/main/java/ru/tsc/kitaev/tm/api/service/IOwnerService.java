package ru.tsc.kitaev.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kitaev.tm.model.AbstractOwnerEntity;

import java.sql.SQLException;
import java.util.List;

public interface IOwnerService<E extends AbstractOwnerEntity> extends IService<E> {

    void remove(@Nullable final String userId, @Nullable final E entity) throws SQLException;

    void clear(@Nullable final String userId) throws SQLException;

    @NotNull
    List<E> findAll(@Nullable final String userId) throws SQLException;

    @NotNull
    List<E> findAll(@Nullable final String userId, @Nullable final String sort) throws SQLException;

    @Nullable
    E findById(@Nullable final String userId, @Nullable final String id) throws SQLException;

    @NotNull
    E findByIndex(@Nullable final String userId, @Nullable final Integer index) throws SQLException;

    void removeById(@Nullable final String userId, @Nullable final String id) throws SQLException;

    void removeByIndex(@Nullable final String userId, @Nullable final Integer index) throws SQLException;

    boolean existsById(@Nullable final String userId, @Nullable final String id) throws SQLException;

    boolean existsByIndex(@Nullable final String userId, final int index) throws SQLException;

    @NotNull
    Integer getSize(@Nullable final String userId) throws SQLException;

}
