package ru.tsc.kitaev.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kitaev.tm.enumerated.Status;
import ru.tsc.kitaev.tm.model.Project;

import java.sql.SQLException;
import java.util.List;

public interface IProjectService extends IOwnerService<Project> {

    void create(@Nullable final String userId, @Nullable final String name) throws SQLException;

    @NotNull
    Project create(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description
    ) throws SQLException;

    void addAll(@NotNull final List<Project> projects) throws SQLException;

    @NotNull
    Project findByName(@Nullable final String userId, @Nullable final String name) throws SQLException;

    void removeByName(@Nullable final String userId, @Nullable final String name) throws SQLException;

    void updateById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @NotNull final String description
    ) throws SQLException;

    void updateByIndex(
            @Nullable String userId,
            @Nullable Integer index,
            @Nullable String name,
            @NotNull String description
    ) throws SQLException;

    void startById(@Nullable final String userId, @Nullable final String id) throws SQLException;

    void startByIndex(@Nullable final String userId, @Nullable final Integer index) throws SQLException;

    void startByName(@Nullable final String userId, @Nullable final String name) throws SQLException;

    void finishById(@Nullable final String userId, @Nullable final String id) throws SQLException;

    void finishByIndex(@Nullable final String userId, @Nullable final Integer index) throws SQLException;

    void finishByName(@Nullable final String userId, @Nullable final String name) throws SQLException;

    void changeStatusById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final Status status
    ) throws SQLException;

    void changeStatusByIndex(
            @Nullable final String userId,
            @Nullable final Integer index,
            @Nullable final Status status
    ) throws SQLException;

    void changeStatusByName(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final Status status
    ) throws SQLException;

}
