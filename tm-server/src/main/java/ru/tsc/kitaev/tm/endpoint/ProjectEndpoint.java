package ru.tsc.kitaev.tm.endpoint;

import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kitaev.tm.api.endpoint.IProjectEndpoint;
import ru.tsc.kitaev.tm.api.service.IServiceLocator;
import ru.tsc.kitaev.tm.enumerated.Status;
import ru.tsc.kitaev.tm.exception.AbstractException;
import ru.tsc.kitaev.tm.model.Project;
import ru.tsc.kitaev.tm.model.Session;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
@NoArgsConstructor
public class ProjectEndpoint extends AbstractEndpoint implements IProjectEndpoint {

    public ProjectEndpoint(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    @SneakyThrows
    @WebMethod
    public void removeProject(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "entity", partName = "entity") @Nullable final Project entity
    ) throws AbstractException {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getProjectService().remove(session.getUserId(), entity);
    }

    @Override
    @SneakyThrows
    @WebMethod
    public void clearProject(
            @WebParam(name = "session", partName = "session") @Nullable final Session session
    ) throws AbstractException {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getProjectService().clear(session.getUserId());
    }

    @Override
    @NotNull
    @SneakyThrows
    @WebMethod
    public List<Project> findProjectAll(
            @WebParam(name = "session", partName = "session") @Nullable final Session session
    ) throws AbstractException {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectService().findAll(session.getUserId());
    }

    @Override
    @NotNull
    @SneakyThrows
    @WebMethod
    public List<Project> findProjectAllSorted(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "sort", partName = "sort") @Nullable final String sort
    ) throws AbstractException {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectService().findAll(session.getUserId(), sort);
    }

    @Override
    @Nullable
    @SneakyThrows
    @WebMethod
    public Project findProjectById(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "id", partName = "id") @Nullable final String id
    ) throws AbstractException {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectService().findById(session.getUserId(), id);
    }

    @Override
    @NotNull
    @SneakyThrows
    @WebMethod
    public Project findProjectByIndex(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "index", partName = "index") @Nullable final Integer index
    ) throws AbstractException {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectService().findByIndex(session.getUserId(), index);
    }

    @Override
    @SneakyThrows
    @WebMethod
    public void removeProjectById(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "id", partName = "id") @Nullable final String id
    ) throws AbstractException {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getProjectTaskService().removeById(session.getUserId(), id);
    }

    @Override
    @SneakyThrows
    @WebMethod
    public void removeProjectByIndex(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "index", partName = "index") @Nullable final Integer index
    ) throws AbstractException {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getProjectTaskService().removeByIndex(session.getUserId(), index);
    }

    @Override
    @NotNull
    @SneakyThrows
    @WebMethod
    public Project createProject(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "name", partName = "name") @Nullable final String name,
            @WebParam(name = "description", partName = "description") @Nullable final String description
    ) throws AbstractException {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectService().create(session.getUserId(), name, description);
    }

    @Override
    @NotNull
    @SneakyThrows
    @WebMethod
    public Project findProjectByName(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "name", partName = "name") @Nullable final String name
    ) throws AbstractException {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectService().findByName(session.getUserId(), name);
    }

    @Override
    @SneakyThrows
    @WebMethod
    public void removeProjectByName(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "name", partName = "name") @Nullable final String name
    ) throws AbstractException {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getProjectTaskService().removeByName(session.getUserId(), name);
    }

    @Override
    @SneakyThrows
    @WebMethod
    public void updateProjectById(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "id", partName = "id") @Nullable final String id,
            @WebParam(name = "name", partName = "name") @Nullable final String name,
            @WebParam(name = "description", partName = "description") @NotNull final String description
    ) throws AbstractException {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getProjectService().updateById(session.getUserId(), id, name, description);
    }

    @Override
    @SneakyThrows
    @WebMethod
    public void updateProjectByIndex(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "index", partName = "index") @Nullable final Integer index,
            @WebParam(name = "name", partName = "name") @Nullable final String name,
            @WebParam(name = "description", partName = "description") @NotNull final String description
    ) throws AbstractException {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getProjectService().updateByIndex(session.getUserId(), index, name, description);
    }

    @Override
    @SneakyThrows
    @WebMethod
    public void startProjectById(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "id", partName = "id") @Nullable final String id
    ) throws AbstractException {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getProjectService().startById(session.getUserId(), id);
    }

    @Override
    @SneakyThrows
    @WebMethod
    public void startProjectByIndex(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "index", partName = "index") @Nullable final Integer index
    ) throws AbstractException {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getProjectService().startByIndex(session.getUserId(), index);
    }

    @Override
    @SneakyThrows
    @WebMethod
    public void startProjectByName(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "name", partName = "name") @Nullable final String name
    ) throws AbstractException {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getProjectService().startByName(session.getUserId(), name);
    }

    @Override
    @SneakyThrows
    @WebMethod
    public void finishProjectById(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "id", partName = "id") @Nullable final String id
    ) throws AbstractException {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getProjectService().finishById(session.getUserId(), id);
    }

    @Override
    @SneakyThrows
    @WebMethod
    public void finishProjectByIndex(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "index", partName = "index") @Nullable final Integer index
    ) throws AbstractException {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getProjectService().finishByIndex(session.getUserId(), index);
    }

    @Override
    @SneakyThrows
    @WebMethod
    public void finishProjectByName(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "name", partName = "name") @Nullable final String name
    ) throws AbstractException {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getProjectService().finishByName(session.getUserId(), name);
    }

    @Override
    @SneakyThrows
    @WebMethod
    public void changeProjectStatusById(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "id", partName = "id") @Nullable final String id,
            @WebParam(name = "status", partName = "status") @Nullable final Status status
    ) throws AbstractException {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getProjectService().changeStatusById(session.getUserId(), id, status);
    }

    @Override
    @SneakyThrows
    @WebMethod
    public void changeProjectStatusByIndex(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "index", partName = "index") @Nullable final Integer index,
            @WebParam(name = "status", partName = "status") @Nullable final Status status
    ) throws AbstractException {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getProjectService().changeStatusByIndex(session.getUserId(), index, status);
    }

    @Override
    @SneakyThrows
    @WebMethod
    public void changeProjectStatusByName(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "name", partName = "name") @Nullable final String name,
            @WebParam(name = "status", partName = "status") @Nullable final Status status
    ) throws AbstractException {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getProjectService().changeStatusByName(session.getUserId(), name, status);
    }

    @Override
    @SneakyThrows
    @WebMethod
    public boolean existsProjectById(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "id", partName = "id") @Nullable final String id
    ) throws AbstractException {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectService().existsById(session.getUserId(), id);
    }

    @Override
    @SneakyThrows
    @WebMethod
    public boolean existsProjectByIndex(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "index", partName = "index") final int index
    ) throws AbstractException {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectService().existsByIndex(session.getUserId(), index);
    }

}
