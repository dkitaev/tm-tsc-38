package ru.tsc.kitaev.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kitaev.tm.api.repository.IOwnerRepository;
import ru.tsc.kitaev.tm.enumerated.Sort;
import ru.tsc.kitaev.tm.exception.entity.EntityNotFoundException;
import ru.tsc.kitaev.tm.model.AbstractOwnerEntity;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public abstract class AbstractOwnerRepository<E extends AbstractOwnerEntity> extends AbstractRepository<E>
        implements IOwnerRepository<E> {

    public AbstractOwnerRepository(@NotNull final Connection connection) {
        super(connection);
    }

    @Override
    public void remove(@NotNull final String userId, @NotNull final E entity) throws SQLException {
        @NotNull final String id = entity.getId();
        @NotNull final String query = "DELETE FROM " + getTableName() + " WHERE user_id = ? AND id = ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, userId);
        statement.setString(2, id);
        statement.executeUpdate();
        statement.close();
    }

    @Override
    public void clear(@NotNull final String userId) throws SQLException {
        @NotNull final String query = "DELETE FROM " + getTableName() + " WHERE user_id = ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, userId);
        statement.executeUpdate();
        statement.close();
    }

    @NotNull
    @Override
    public List<E> findAll(@NotNull final String userId) throws SQLException {
        @NotNull final String query = "SELECT * FROM " + getTableName() + " WHERE user_id = ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, userId);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        @NotNull final List<E> result = new ArrayList<>();
        while (resultSet.next()) result.add(fetch(resultSet));
        statement.close();
        if (result.isEmpty()) throw new EntityNotFoundException();
        return result;
    }

    @NotNull
    @Override
    public List<E> findAll(@NotNull final String userId, @NotNull final String sort) throws SQLException {
        @NotNull final String query = "SELECT * FROM " + getTableName() + " WHERE user_id = ? ORDER BY ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, userId);
        statement.setString(2, Sort.valueOf(sort).name());
        @NotNull final ResultSet resultSet = statement.executeQuery();
        @NotNull final List<E> result = new ArrayList<>();
        while (resultSet.next()) result.add(fetch(resultSet));
        statement.close();
        if (result.isEmpty()) throw new EntityNotFoundException();
        return result;
    }

    @Nullable
    @Override
    public E findById(@NotNull final String userId, @NotNull final String id) throws SQLException {
        @NotNull final String query = "SELECT * FROM " + getTableName() + " WHERE user_id = ? AND id = ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, userId);
        statement.setString(2, id);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        if (!resultSet.next()) throw new EntityNotFoundException();
        @NotNull final E result = fetch(resultSet);
        statement.close();
        return result;
    }

    @NotNull
    @Override
    public E findByIndex(@NotNull final String userId, @NotNull final Integer index) throws SQLException {
        @NotNull final String query = "SELECT * FROM " + getTableName() + " WHERE user_id = ? LIMIT 1 OFFSET ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, userId);
        statement.setInt(2, index);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        if (!resultSet.next()) throw new EntityNotFoundException();
        @NotNull final E result = fetch(resultSet);
        statement.close();
        return result;
    }

    @Override
    public void removeById(@NotNull final String userId, @NotNull final String id) throws SQLException {
        @NotNull final String query = "DELETE FROM " + getTableName() + " WHERE user_id = ? AND id = ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, userId);
        statement.setString(2, id);
        statement.executeUpdate();
        statement.close();
    }

    @Override
    public void removeByIndex(@NotNull final String userId, @NotNull final Integer index) throws SQLException {
        @NotNull final String id = findByIndex(userId, index).getId();
        removeById(userId, id);
    }

    @Override
    public boolean existsById(@NotNull final String userId, @NotNull final String id) throws SQLException {
        @Nullable final E entity = findById(userId, id);
        return entity != null;
    }

    @Override
    public boolean existsByIndex(@NotNull final String userId, final int index) throws SQLException {
        findByIndex(userId, index);
        return true;
    }

    @NotNull
    @Override
    public Integer getSize(@NotNull final String userId) throws SQLException {
        @NotNull final String query = "SELECT count(*) AS count FROM " + getTableName() + " WHERE user_id = ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, userId);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        resultSet.next();
        return resultSet.getInt("count");
    }

}
