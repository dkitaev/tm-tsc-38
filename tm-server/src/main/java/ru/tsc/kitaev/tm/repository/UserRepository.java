package ru.tsc.kitaev.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kitaev.tm.api.repository.IUserRepository;
import ru.tsc.kitaev.tm.enumerated.Role;
import ru.tsc.kitaev.tm.exception.entity.EntityNotFoundException;
import ru.tsc.kitaev.tm.exception.user.LoginExistsException;
import ru.tsc.kitaev.tm.model.Project;
import ru.tsc.kitaev.tm.model.User;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.function.Predicate;

public final class UserRepository extends AbstractRepository<User> implements IUserRepository {

    @NotNull
    private final static String USER_TABLE = "users";

    public UserRepository(@NotNull final Connection connection) {
        super(connection);
    }

    @NotNull
    @Override
    protected String getTableName() {
        return USER_TABLE;
    }

    @NotNull
    @Override
    protected User fetch(@NotNull ResultSet row) throws SQLException {
        @NotNull final User user = new User();
        user.setId(row.getString("id"));
        user.setLogin(row.getString("login"));
        user.setPasswordHash(row.getString("password_hash"));
        user.setEmail(row.getString("email"));
        user.setFirstName(row.getString("first_name"));
        user.setLastName(row.getString("last_name"));
        user.setMiddleName(row.getString("middle_name"));
        user.setRole(Role.valueOf(row.getString("role")));
        user.setLocked(row.getBoolean("locked"));
        return user;
    }

    @Override
    public void add(@NotNull final User user) throws SQLException {
        @NotNull final String query = "INSERT INTO " + getTableName() +
                " (id, login, password_hash, email, first_name, last_name, middle_name, role, locked)" +
                " VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, user.getId());
        statement.setString(2, user.getLogin());
        statement.setString(3, user.getPasswordHash());
        statement.setString(4, user.getEmail());
        statement.setString(5, user.getFirstName());
        statement.setString(6, user.getLastName());
        statement.setString(7, user.getMiddleName());
        statement.setString(8, user.getRole().toString());
        statement.setBoolean(9, user.getLocked());
        statement.executeUpdate();
        statement.close();
    }

    @Override
    public void addAll(@NotNull List<User> users) throws SQLException {
        for (User user : users) {
            add(user);
        }
    }

    @Override
    public void removeUser(@NotNull final User user) throws SQLException {
        @NotNull final String id = user.getId();
        @NotNull final String query = "DELETE FROM " + getTableName() + " WHERE id = ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, id);
        statement.executeUpdate();
        statement.close();
    }

    @NotNull
    @Override
    public User findByLogin(@NotNull final String login) throws SQLException {
        @NotNull final String query = "SELECT * FROM " + getTableName() + " WHERE login = ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, login);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        if (!resultSet.next()) throw new EntityNotFoundException();
        @NotNull final User result = fetch(resultSet);
        statement.close();
        return result;
    }

    @NotNull
    @Override
    public User findByEmail(@NotNull final String email) throws SQLException {
        @NotNull final String query = "SELECT * FROM " + getTableName() + " WHERE email = ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, email);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        if (!resultSet.next()) throw new EntityNotFoundException();
        @NotNull final User result = fetch(resultSet);
        statement.close();
        return result;
    }

    @Override
    public void removeByLogin(@NotNull final String login) throws SQLException {
        @Nullable final User user = findByLogin(login);
        removeUser(user);
    }

    @Override
    public void updatePassword(@NotNull final String id, @NotNull final String password) throws SQLException {
        @NotNull final String query = "UPDATE " + getTableName() + " SET password_hash = ? WHERE id = ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, password);
        statement.setString(2, id);
        statement.executeUpdate();
        statement.close();
    }

    @Override
    public void updateLockUserByLogin(@NotNull final String login, final boolean locked) throws SQLException {
        @NotNull final String query = "UPDATE " + getTableName() + " SET locked = ? WHERE login = ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setBoolean(1, locked);
        statement.setString(2, login);
        statement.executeUpdate();
        statement.close();
    }

    @Override
    public void updateUser(
            @NotNull final String id,
            @NotNull final String firstName,
            @NotNull final String lastName,
            @NotNull final String middleName
    ) throws SQLException {
        @NotNull final String query = "UPDATE " + getTableName() +
                " SET first_name = ?, last_name = ?, middle_name = ? WHERE id = ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, firstName);
        statement.setString(2, lastName);
        statement.setString(3, middleName);
        statement.setString(4, id);
        statement.executeUpdate();
        statement.close();
    }

    @NotNull
    @Override
    public Boolean isLoginExists(@NotNull final String login) throws SQLException {
        try {
            findByLogin(login);
            return true;
        } catch (EntityNotFoundException e) {
            return false;
        }
    }

    @NotNull
    @Override
    public Boolean isEmailExists(@NotNull final String email) throws SQLException {
        try {
            findByLogin(email);
            return true;
        } catch (EntityNotFoundException e) {
            return false;
        }
    }

}
