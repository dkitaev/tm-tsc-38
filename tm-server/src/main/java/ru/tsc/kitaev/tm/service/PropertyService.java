package ru.tsc.kitaev.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kitaev.tm.api.service.IPropertyService;

import java.io.InputStream;
import java.util.Properties;

public final class PropertyService implements IPropertyService {

    @NotNull
    private static final String FILE_NAME = "config.properties";

    @NotNull
    private static final String APPLICATION_VERSION_KEY = "application.version";

    @NotNull
    private static final String APPLICATION_VERSION_DEFAULT = "";

    @NotNull
    private static final String PASSWORD_ITERATION_KEY = "password.iteration";

    @NotNull
    private static final String PASSWORD_ITERATION_DEFAULT = "1";

    @NotNull
    private static final String PASSWORD_SECRET_KEY = "password.secret";

    @NotNull
    private static final String PASSWORD_SECRET_DEFAULT = "";

    @NotNull
    private static final String SESSION_SECRET_KEY = "session.secret";

    @NotNull
    private static final String SESSION_SECRET_DEFAULT = "";

    @NotNull
    private static final String SESSION_ITERATION_KEY = "session.iteration";

    @NotNull
    private static final String SESSION_ITERATION_DEFAULT = "1";

    @NotNull
    private static final String DEVELOPER_NAME = "developer.name";

    @NotNull
    private static final String DEVELOPER_NAME_DEFAULT = "";

    @NotNull
    private static final String DEVELOPER_EMAIL = "developer.email";

    @NotNull
    private static final String DEVELOPER_EMAIL_DEFAULT = "";

    @NotNull
    private static final String SERVER_HOST_KEY = "server.host";

    @NotNull
    private static final String SERVER_HOST_DEFAULT_VALUE = "localhost";

    @NotNull
    private static final String SERVER_PORT_KEY = "server.port";

    @NotNull
    private static final String SERVER_PORT_DEFAULT_VALUE = "8080";

    @NotNull
    private static final String JDBC_USER_KEY = "jdbc.user";

    @NotNull
    private static final String JDBC_USER_DEFAULT_VALUE = "postgres";

    @NotNull
    private static final String JDBC_USER_PASSWORD_KEY = "jdbc.password";

    @NotNull
    private static final String JDBC_USER_PASSWORD_DEFAULT_VALUE = "postgres";

    @NotNull
    private final static String JDBC_URL_KEY = "jdbc.url";

    @NotNull
    private final static String JDBC_URL_DEFAULT_VALUE = "jdbc:postgresql://localhost/task-manager";

    @NotNull
    private final Properties properties = new Properties();

    @SneakyThrows
    public PropertyService() {
        @Nullable final InputStream inputStream = ClassLoader.getSystemResourceAsStream(FILE_NAME);
        if (inputStream == null) return;
        properties.load(inputStream);
        inputStream.close();
    }

    @NotNull
    public String getValue(final String value, final String defaultValue) {
        @Nullable final String systemProperty = System.getProperty(value);
        if (systemProperty != null) return systemProperty;
        @Nullable final String environmentProperty = System.getenv(value);
        if (environmentProperty != null) return environmentProperty;
        return properties.getProperty(value, defaultValue);
    }

    @NotNull
    public Integer getValueInt(final String value, final String defaultValue) {
        @Nullable final String systemProperty = System.getProperty(value);
        if (systemProperty != null) return Integer.parseInt(systemProperty);
        @Nullable final String environmentProperty = System.getenv(value);
        if (environmentProperty != null) return Integer.parseInt(environmentProperty);
        return Integer.parseInt(properties.getProperty(value, defaultValue));
    }

    @NotNull
    @Override
    public String getPasswordSecret() {
        return getValue(PASSWORD_SECRET_KEY, PASSWORD_SECRET_DEFAULT);
    }

    @NotNull
    @Override
    public Integer getPasswordIteration() {
        return getValueInt(PASSWORD_ITERATION_KEY, PASSWORD_ITERATION_DEFAULT);
    }

    @NotNull
    @Override
    public String getSessionSecret() {
        return getValue(SESSION_SECRET_KEY, SESSION_SECRET_DEFAULT);
    }

    @NotNull
    @Override
    public Integer getSessionIteration() {
        return getValueInt(SESSION_ITERATION_KEY, SESSION_ITERATION_DEFAULT);
    }

    @NotNull
    @Override
    public String getApplicationVersion() {
        return getValue(APPLICATION_VERSION_KEY, APPLICATION_VERSION_DEFAULT);
    }

    @NotNull
    @Override
    public String getDeveloperName() {
        return getValue(DEVELOPER_NAME, DEVELOPER_NAME_DEFAULT);
    }

    @NotNull
    @Override
    public String getDeveloperEmail() {
        return getValue(DEVELOPER_EMAIL, DEVELOPER_EMAIL_DEFAULT);
    }

    @Override
    public @NotNull String getServerHost() {
        return getValue(SERVER_HOST_KEY, SERVER_HOST_DEFAULT_VALUE);
    }

    @NotNull
    @Override
    public Integer getServerPort() {
        return getValueInt(SERVER_PORT_KEY, SERVER_PORT_DEFAULT_VALUE);
    }

    @NotNull
    @Override
    public String getJdbcUser() {
        return getValue(JDBC_USER_KEY, JDBC_USER_DEFAULT_VALUE);
    }

    @NotNull
    @Override
    public String getJdbcPassword() {
        return getValue(JDBC_USER_PASSWORD_KEY, JDBC_USER_PASSWORD_DEFAULT_VALUE);
    }

    @NotNull
    @Override
    public String getJdbcUrl() {
        return getValue(JDBC_URL_KEY, JDBC_URL_DEFAULT_VALUE);
    }

}
