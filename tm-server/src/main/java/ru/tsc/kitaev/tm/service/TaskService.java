package ru.tsc.kitaev.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kitaev.tm.api.repository.ITaskRepository;
import ru.tsc.kitaev.tm.api.service.IConnectionService;
import ru.tsc.kitaev.tm.api.service.ILogService;
import ru.tsc.kitaev.tm.api.service.ITaskService;
import ru.tsc.kitaev.tm.enumerated.Status;
import ru.tsc.kitaev.tm.exception.empty.*;
import ru.tsc.kitaev.tm.exception.entity.TaskNotFoundException;
import ru.tsc.kitaev.tm.exception.system.DatabaseOperationException;
import ru.tsc.kitaev.tm.model.Task;
import ru.tsc.kitaev.tm.repository.TaskRepository;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

public final class TaskService extends AbstractOwnerService<Task> implements ITaskService {

    public TaskService(
            @NotNull final IConnectionService connectionService,
            @NotNull final ILogService logService
    ) {
        super(connectionService, logService);
    }

    @NotNull
    public ITaskRepository getRepository(@NotNull final Connection connection) {
        return new TaskRepository(connection);
    }

    @Override
    public void create(@Nullable final String userId, @Nullable final String name) throws SQLException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NotNull final Task task = new Task();
        task.setUserId(userId);
        task.setName(name);
        @NotNull final Connection connection = connectionService.getConnection();
        @NotNull final ITaskRepository taskRepository = getRepository(connection);
        try {
            taskRepository.add(userId, task);
            connection.commit();
        } catch (SQLException e) {
            logService.error(e);
            throw new DatabaseOperationException();
        } finally {
            connection.close();
        }
    }

    @NotNull
    @Override
    public Task create(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description
    ) throws SQLException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (description == null || description.isEmpty()) throw new EmptyDescriptionException();
        @NotNull final Task task = new Task();
        task.setUserId(userId);
        task.setName(name);
        task.setDescription(description);
        @NotNull final Connection connection = connectionService.getConnection();
        @NotNull final ITaskRepository taskRepository = getRepository(connection);
        try {
            taskRepository.add(userId, task);
            connection.commit();
        } catch (SQLException e) {
            logService.error(e);
            throw new DatabaseOperationException();
        } finally {
            connection.close();
        }
        return task;
    }

    @Override
    public void addAll(@NotNull final List<Task> tasks) throws SQLException {
        @NotNull final Connection connection = connectionService.getConnection();
        @NotNull final ITaskRepository taskRepository = getRepository(connection);
        try {
            taskRepository.addAll(tasks);
            connection.commit();
        } catch (SQLException e) {
            logService.error(e);
            throw new DatabaseOperationException();
        } finally {
            connection.close();
        }
    }

    @NotNull
    @Override
    public Task findByName(@Nullable final String userId, @Nullable final String name) throws SQLException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NotNull final Connection connection = connectionService.getConnection();
        @NotNull final ITaskRepository taskRepository = getRepository(connection);
        try {
            return taskRepository.findByName(userId, name);
        } catch (SQLException e) {
            logService.error(e);
            throw new DatabaseOperationException();
        } finally {
            connection.close();
        }
    }

    @Override
    public void removeByName(@Nullable final String userId, @Nullable final String name) throws SQLException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NotNull final Connection connection = connectionService.getConnection();
        @NotNull final ITaskRepository taskRepository = getRepository(connection);
        try {
            taskRepository.removeByName(userId, name);
            connection.commit();
        } catch (SQLException e) {
            logService.error(e);
            throw new DatabaseOperationException();
        } finally {
            connection.close();
        }
    }

    @Override
    public void updateById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @NotNull final String description
    ) throws SQLException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NotNull final Connection connection = connectionService.getConnection();
        @NotNull final ITaskRepository taskRepository = getRepository(connection);
        try {
            taskRepository.updateById(userId, id, name, description);
            connection.commit();
        } catch (SQLException e) {
            logService.error(e);
            throw new DatabaseOperationException();
        } finally {
            connection.close();
        }
    }

    @Override
    public void updateByIndex(
            @Nullable final String userId,
            @Nullable final Integer index,
            @Nullable final String name,
            @NotNull final String description
    ) throws SQLException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (index == null || index < 0) throw new EmptyIndexException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NotNull final Connection connection = connectionService.getConnection();
        @NotNull final ITaskRepository taskRepository = getRepository(connection);
        try {
            taskRepository.updateByIndex(userId, index, name, description);
            connection.commit();
        } catch (SQLException e) {
            logService.error(e);
            throw new DatabaseOperationException();
        } finally {
            connection.close();
        }
    }

    @Override
    public void startById(@Nullable final String userId, @Nullable final String id) throws SQLException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @NotNull final Connection connection = connectionService.getConnection();
        @NotNull final ITaskRepository taskRepository = getRepository(connection);
        try {
            taskRepository.startById(userId, id);
            connection.commit();
        } catch (SQLException e) {
            logService.error(e);
            throw new DatabaseOperationException();
        } finally {
            connection.close();
        }
    }

    @Override
    public void startByIndex(@Nullable final String userId, @Nullable final Integer index) throws SQLException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (index == null || index < 0) throw new EmptyIndexException();
        @NotNull final Connection connection = connectionService.getConnection();
        @NotNull final ITaskRepository taskRepository = getRepository(connection);
        try {
            taskRepository.startByIndex(userId, index);
            connection.commit();
        } catch (SQLException e) {
            logService.error(e);
            throw new DatabaseOperationException();
        } finally {
            connection.close();
        }
    }

    @Override
    public void startByName(@Nullable final String userId, @Nullable final String name) throws SQLException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NotNull final Connection connection = connectionService.getConnection();
        @NotNull final ITaskRepository taskRepository = getRepository(connection);
        try {
            taskRepository.startByName(userId, name);
            connection.commit();
        } catch (SQLException e) {
            logService.error(e);
            throw new DatabaseOperationException();
        } finally {
            connection.close();
        }
    }

    @Override
    public void finishById(@Nullable final String userId, @Nullable final String id) throws SQLException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @NotNull final Connection connection = connectionService.getConnection();
        @NotNull final ITaskRepository taskRepository = getRepository(connection);
        try {
            taskRepository.finishById(userId, id);
            connection.commit();
        } catch (SQLException e) {
            logService.error(e);
            throw new DatabaseOperationException();
        } finally {
            connection.close();
        }
    }

    @Override
    public void finishByIndex(@Nullable final String userId, @Nullable final Integer index) throws SQLException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (index == null || index < 0) throw new EmptyIndexException();
        @NotNull final Connection connection = connectionService.getConnection();
        @NotNull final ITaskRepository taskRepository = getRepository(connection);
        if (taskRepository.getSize() < index - 1) throw new TaskNotFoundException();
        try {
            taskRepository.finishByIndex(userId, index);
            connection.commit();
        } catch (SQLException e) {
            logService.error(e);
            throw new DatabaseOperationException();
        } finally {
            connection.close();
        }
    }

    @Override
    public void finishByName(@Nullable final String userId, @Nullable final String name) throws SQLException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NotNull final Connection connection = connectionService.getConnection();
        @NotNull final ITaskRepository taskRepository = getRepository(connection);
        try {
            taskRepository.finishByName(userId, name);
            connection.commit();
        } catch (SQLException e) {
            logService.error(e);
            throw new DatabaseOperationException();
        } finally {
            connection.close();
        }
    }

    @Override
    public void changeStatusById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final Status status
    ) throws SQLException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (status == null) throw new EmptyStatusException();
        @NotNull final Connection connection = connectionService.getConnection();
        @NotNull final ITaskRepository taskRepository = getRepository(connection);
        try {
            taskRepository.changeStatusById(userId, id, status);
            connection.commit();
        } catch (SQLException e) {
            logService.error(e);
            throw new DatabaseOperationException();
        } finally {
            connection.close();
        }
    }

    @Override
    public void changeStatusByIndex(
            @Nullable final String userId,
            @Nullable final Integer index,
            @Nullable final Status status
    ) throws SQLException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (index == null || index < 0) throw new EmptyIndexException();
        if (status == null) throw new EmptyStatusException();
        @NotNull final Connection connection = connectionService.getConnection();
        @NotNull final ITaskRepository taskRepository = getRepository(connection);
        try {
            taskRepository.changeStatusByIndex(userId, index, status);
            connection.commit();
        } catch (SQLException e) {
            logService.error(e);
            throw new DatabaseOperationException();
        } finally {
            connection.close();
        }
    }

    @Override
    public void changeStatusByName(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final Status status
    ) throws SQLException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (status == null) throw new EmptyStatusException();
        @NotNull final Connection connection = connectionService.getConnection();
        @NotNull final ITaskRepository taskRepository = getRepository(connection);
        try {
            taskRepository.changeStatusByName(userId, name, status);
            connection.commit();
        } catch (SQLException e) {
            logService.error(e);
            throw new DatabaseOperationException();
        } finally {
            connection.close();
        }
    }

}
