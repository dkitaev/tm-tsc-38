package ru.tsc.kitaev.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.tsc.kitaev.tm.api.repository.IProjectRepository;
import ru.tsc.kitaev.tm.api.repository.IUserRepository;
import ru.tsc.kitaev.tm.api.service.IConnectionService;
import ru.tsc.kitaev.tm.api.service.IPropertyService;
import ru.tsc.kitaev.tm.enumerated.Status;
import ru.tsc.kitaev.tm.model.Project;
import ru.tsc.kitaev.tm.model.User;
import ru.tsc.kitaev.tm.service.ConnectionService;
import ru.tsc.kitaev.tm.service.PropertyService;
import ru.tsc.kitaev.tm.util.HashUtil;

import java.sql.Connection;
import java.sql.SQLException;

public class ProjectRepositoryTest {

    @NotNull
    private final Connection connection;

    @NotNull
    private final IProjectRepository projectRepository;

    @NotNull
    private final IUserRepository userRepository;

    @NotNull
    private final Project project;

    @NotNull
    private final String projectId;

    @NotNull
    private final String projectName = "testProject";

    @NotNull
    private final String projectDescription = "testProjectDescription";

    @NotNull
    private final String userId;

    public ProjectRepositoryTest() throws SQLException {
        @NotNull final IPropertyService propertyService = new PropertyService();
        @NotNull final IConnectionService connectionService = new ConnectionService(propertyService);
        connection = connectionService.getConnection();
        projectRepository = new ProjectRepository(connection);
        userRepository = new UserRepository(connection);
        @NotNull final User user = new User();
        userId = user.getId();
        user.setLogin("test");
        user.setPasswordHash(HashUtil.salt("test", 5, "test"));
        userRepository.add(user);
        project = new Project();
        projectId = project.getId();
        project.setUserId(userId);
        project.setName(projectName);
        project.setDescription(projectDescription);
        connection.commit();
    }

    @Before
    public void before() throws SQLException {
        projectRepository.add(userId, project);
        connection.commit();
    }

    @Test
    public void findProjectTest() throws SQLException {
        Assert.assertNotNull(project);
        Assert.assertNotNull(userId);
        Assert.assertNotNull(projectId);
        Assert.assertNotNull(projectName);
        Assert.assertEquals(project, projectRepository.findById(userId, projectId));
        Assert.assertEquals(project, projectRepository.findByIndex(userId, 0));
        Assert.assertEquals(project, projectRepository.findByName(userId, projectName));
    }

    @Test
    public void removeProjectByIdTest() throws SQLException {
        Assert.assertNotNull(project);
        Assert.assertNotNull(userId);
        Assert.assertNotNull(projectId);
        projectRepository.removeById(userId, projectId);
        connection.commit();
        Assert.assertTrue(projectRepository.findAll().isEmpty());
    }

    @Test
    public void removeProjectByIndexTest() throws SQLException {
        Assert.assertNotNull(project);
        Assert.assertNotNull(userId);
        projectRepository.removeByIndex(userId, 0);
        connection.commit();
        Assert.assertTrue(projectRepository.findAll().isEmpty());
    }

    @Test
    public void removeProjectByNameTest() throws SQLException {
        Assert.assertNotNull(project);
        Assert.assertNotNull(userId);
        Assert.assertNotNull(projectName);
        projectRepository.removeByName(userId, projectName);
        connection.commit();
        Assert.assertTrue(projectRepository.findAll().isEmpty());
    }

    @Test
    public void updateByIdTest() throws SQLException {
        @NotNull final String newName = "newTestProject";
        @NotNull final String newDescription = "newTestProjectDescription";
        projectRepository.updateById(userId, projectId, newName, newDescription);
        connection.commit();
        Assert.assertEquals(newName, projectRepository.findById(userId, projectId).getName());
        Assert.assertEquals(newDescription, projectRepository.findById(userId, projectId).getDescription());
        Assert.assertNotEquals(projectName, projectRepository.findById(userId, projectId).getName());
        Assert.assertNotEquals(projectDescription, projectRepository.findById(userId, projectId).getDescription());
    }

    @Test
    public void updateByIndexTest() throws SQLException {
        @NotNull final String newName = "newTestProject";
        @NotNull final String newDescription = "newTestProjectDescription";
        projectRepository.updateByIndex(userId, 0, newName, newDescription);
        connection.commit();
        Assert.assertEquals(newName, projectRepository.findById(userId, projectId).getName());
        Assert.assertEquals(newDescription, projectRepository.findById(userId, projectId).getDescription());
        Assert.assertNotEquals(projectName, projectRepository.findById(userId, projectId).getName());
        Assert.assertNotEquals(projectDescription, projectRepository.findById(userId, projectId).getDescription());
    }

    @Test
    public void startByIdTest() throws SQLException {
        Assert.assertNotNull(userId);
        Assert.assertNotNull(projectId);
        projectRepository.startById(userId, projectId);
        connection.commit();
        Assert.assertEquals(Status.IN_PROGRESS, projectRepository.findById(userId, projectId).getStatus());
    }

    @Test
    public void startByIndexTest() throws SQLException {
        Assert.assertNotNull(userId);
        projectRepository.startByIndex(userId, 0);
        connection.commit();
        Assert.assertEquals(Status.IN_PROGRESS, projectRepository.findByIndex(userId, 0).getStatus());
    }

    @Test
    public void startByNameTest() throws SQLException {
        Assert.assertNotNull(userId);
        Assert.assertNotNull(projectName);
        projectRepository.startByName(userId, projectName);
        connection.commit();
        Assert.assertEquals(Status.IN_PROGRESS, projectRepository.findByName(userId, projectName).getStatus());
    }

    @Test
    public void finishByIdTest() throws SQLException {
        Assert.assertNotNull(userId);
        Assert.assertNotNull(projectId);
        projectRepository.finishById(userId, projectId);
        connection.commit();
        Assert.assertEquals(Status.COMPLETED, projectRepository.findById(userId, projectId).getStatus());
    }

    @Test
    public void finishByIndexTest() throws SQLException {
        Assert.assertNotNull(userId);
        projectRepository.finishByIndex(userId, 0);
        connection.commit();
        Assert.assertEquals(Status.COMPLETED, projectRepository.findByIndex(userId, 0).getStatus());
    }

    @Test
    public void finishByNameTest() throws SQLException {
        Assert.assertNotNull(userId);
        Assert.assertNotNull(projectName);
        projectRepository.finishByName(userId, projectName);
        connection.commit();
        Assert.assertEquals(Status.COMPLETED, projectRepository.findByName(userId, projectName).getStatus());
    }

    @Test
    public void changeStatusByIdTest() throws SQLException {
        Assert.assertNotNull(userId);
        Assert.assertNotNull(projectId);
        projectRepository.changeStatusById(userId, projectId, Status.IN_PROGRESS);
        connection.commit();
        Assert.assertEquals(Status.IN_PROGRESS, projectRepository.findById(userId, projectId).getStatus());
        projectRepository.changeStatusById(userId, projectId, Status.COMPLETED);
        connection.commit();
        Assert.assertEquals(Status.COMPLETED, projectRepository.findById(userId, projectId).getStatus());
        projectRepository.changeStatusById(userId, projectId, Status.NOT_STARTED);
        connection.commit();
        Assert.assertEquals(Status.NOT_STARTED, projectRepository.findById(userId, projectId).getStatus());
    }

    @Test
    public void changeStatusByIndexTest() throws SQLException {
        Assert.assertNotNull(userId);
        projectRepository.changeStatusByIndex(userId, 0, Status.IN_PROGRESS);
        connection.commit();
        Assert.assertEquals(Status.IN_PROGRESS, projectRepository.findByIndex(userId, 0).getStatus());
        projectRepository.changeStatusByIndex(userId, 0, Status.COMPLETED);
        connection.commit();
        Assert.assertEquals(Status.COMPLETED, projectRepository.findByIndex(userId, 0).getStatus());
        projectRepository.changeStatusByIndex(userId, 0, Status.NOT_STARTED);
        connection.commit();
        Assert.assertEquals(Status.NOT_STARTED, projectRepository.findByIndex(userId, 0).getStatus());
    }

    @Test
    public void changeStatusByNameTest() throws SQLException {
        Assert.assertNotNull(userId);
        Assert.assertNotNull(projectName);
        projectRepository.changeStatusByName(userId, projectName, Status.IN_PROGRESS);
        connection.commit();
        Assert.assertEquals(Status.IN_PROGRESS, projectRepository.findByName(userId, projectName).getStatus());
        projectRepository.changeStatusByName(userId, projectName, Status.COMPLETED);
        connection.commit();
        Assert.assertEquals(Status.COMPLETED, projectRepository.findByName(userId, projectName).getStatus());
        projectRepository.changeStatusByName(userId, projectName, Status.NOT_STARTED);
        connection.commit();
        Assert.assertEquals(Status.NOT_STARTED, projectRepository.findByName(userId, projectName).getStatus());
    }

    @After
    public void after() throws SQLException {
        projectRepository.clear(userId);
        userRepository.removeById(userId);
        connection.commit();
        connection.close();
    }

}
