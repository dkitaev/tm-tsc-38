package ru.tsc.kitaev.tm.service;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.tsc.kitaev.tm.api.service.ISessionService;
import ru.tsc.kitaev.tm.component.Bootstrap;
import ru.tsc.kitaev.tm.enumerated.Role;
import ru.tsc.kitaev.tm.model.Session;

import java.sql.SQLException;

public class SessionServiceTest {

    @NotNull
    private final ISessionService sessionService;

    @NotNull
    private Session session;


    public SessionServiceTest() {
        sessionService = new SessionService(
                new ConnectionService(new PropertyService()), new LogService(), new Bootstrap()
        );
    }

    @Before
    public void before() throws SQLException {
        @NotNull final String userLogin = "userLogin";
        @NotNull final String userPassword = "userPassword";
        session = sessionService.open(userLogin, userPassword);
    }

    @Test
    public void openTest() throws SQLException {
        final int initialSize = sessionService.getSize();
        @NotNull final Session session = sessionService.open("test", "test");
        Assert.assertEquals(initialSize + 1, sessionService.getSize());
        Assert.assertNotNull(session.getSignature());
    }

    @Test
    public void closeTest() throws SQLException {
        final int initialSize = sessionService.getSize();
        @NotNull final Session session = sessionService.findAll().get(0);
        sessionService.close(session);
        Assert.assertEquals(initialSize - 1, sessionService.getSize());
    }

    @Test
    public void validateTest() throws SQLException {
        @NotNull final Session session = sessionService.open("admin", "admin");
        sessionService.validate(session);
    }

    @Test
    public void validateRoleTest() throws SQLException {
        @NotNull final Session session = sessionService.open("admin", "admin");
        sessionService.validate(session, Role.ADMIN);
    }

    @After
    public void after() throws SQLException {
        sessionService.close(session);
    }

}
